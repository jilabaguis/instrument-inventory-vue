import Vue from 'vue'
import { vsSelect, vsPagination, vsIcon } from 'vuesax'
import 'vuesax/dist/vuesax.css'

Vue.use(vsPagination)
Vue.use(vsIcon)
Vue.use(vsSelect)
