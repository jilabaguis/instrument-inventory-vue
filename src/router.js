import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import p404 from './views/404.vue'
import Cookies from 'js-cookie'
import About from './views/About.vue'
import jwt from 'jsonwebtoken'

import test from '@/components/menus/dropdownMenu.vue'
Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  jwt.verify(Cookies.get('token'), process.env.VUE_APP_PRIVATE_KEY, (err, decoded) => {
    if (decoded) {
      next('/')
      return
    }
  })
  next()
}

const ifAuthenticated = (to, from, next) => {
  jwt.verify(Cookies.get('token'), process.env.VUE_APP_PRIVATE_KEY, err => {
    if (err) {
      next('/Login')
      return
    }
  })
  next()
}

export default new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/Login',
      name: 'Login',
      component: Login,
      beforeEnter: ifNotAuthenticated,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '*',
      name: '404',
      component: p404,
    },
    {
      path: '/test',
      name: 'test',
      component: test,
    },
  ],
})
