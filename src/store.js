/* eslint-disable no-unused-vars */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Cookies from 'js-cookie'
import moment from 'moment'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    instruments: [],
    cur_filters: [],
    all_filters: [],
    page: 1,
    limit: 50,
    srch_obj: {},
    panel: -1,
    sortBy: 'PRODUCT_CLASS:ASC',
    drops: {},
    addEditData: {},
    search_text: '',
    userCredentials: {},
    userDetails: {},
    //To be migrated to the Database
    item_prop: [
      [
        { label: 'Item Code', elm: 'ITEM_CODE', pos: 'xs5', type: 'select', visible: true, readonly: false },
        { label: 'Serial No.', elm: 'SERIAL_NO', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'Principal', elm: 'PRINCIPAL', pos: 'xs2', type: 'select', visible: true, readonly: false },
        { label: 'Product Line', elm: 'PRODUCT_CLASS', pos: 'xs2', type: 'select', visible: true, readonly: false },
      ],
      [
        { label: 'Part No.', elm: 'PART_NO', pos: 'xs3', type: 'text', visible: true, readonly: false },
        { label: 'Description', elm: 'DESCRIPTION', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'Status', elm: 'STATUS', pos: 'xs2', type: 'select', visible: true, readonly: false },
        { label: 'Code', elm: 'CODE', pos: 'xs2', type: 'text', visible: true, readonly: true },
        { label: 'RTG Date', elm: 'RTG_DATE', pos: 'xs2', type: 'date', visible: true, readonly: false },
        { label: 'RTG mos', elm: 'RTG_DATE', pos: 'xs1', type: 'computed', visible: true, readonly: false },
      ],
      [
        { label: 'Supplier', elm: 'SUPPLIER', pos: 'xs5', type: 'select', visible: true, readonly: false },
        { label: 'BN/R', elm: 'BN_R', pos: 'xs2', type: 'select', visible: true, readonly: false },
        { label: 'Acquisition Value', elm: 'LANDED_COST', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'NBV', elm: 'NBV', pos: 'xs2', type: 'text', visible: true, readonly: false },
      ],
      [
        { label: 'Location', elm: 'LOCATION', pos: 'xs5', type: 'select', visible: true, readonly: false },
        { label: 'Thru', elm: 'THRU', pos: 'xs2', type: 'select', visible: true, readonly: false },
        { label: 'Direct/Dealer', elm: 'DIRECT_DEALER', pos: 'xs2', type: 'text', visible: true, readonly: true },
        { label: 'Sector', elm: 'SECTOR', pos: 'xs2', type: 'select', visible: true, readonly: false },
      ],
      [
        { label: 'Address', elm: 'ADDRESS', pos: 'xs5', type: 'text', visible: true, readonly: false },
        { label: 'Sales Agreement', elm: 'SAGMT', pos: 'xs2', type: 'select', visible: true, readonly: false },
        { label: 'STS No.', elm: 'DR_STS_NO', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'STS Date', elm: 'DR_STS_DATE', pos: 'xs2', type: 'date', visible: true, readonly: false },
        { label: 'MII mos', elm: 'DR_STS_DATE', pos: 'xs1', type: 'computed', visible: false, readonly: false },
      ],
      [
        { label: 'Service Team', elm: 'SERVC_TEAM', pos: 'xs3', type: 'select', visible: true, readonly: false },
        { label: 'Sales Representative', elm: 'SALES_REP', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'RR No.', elm: 'RR_NO', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'RR Amount', elm: 'RR_AMOUNT', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'RR Date', elm: 'RR_DATE', pos: 'xs2', type: 'date', visible: true, readonly: false },
        { label: 'AGE mos', elm: 'RR_DATE', pos: 'xs1', type: 'computed', visible: true, readonly: false },
      ],
      [
        { label: 'Remarks', elm: 'REMARKS', pos: 'xs5', type: 'text', visible: true, readonly: false },
        { label: 'Software ver.', elm: 'SOFT_VER', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'Property Tag', elm: 'PROPERTY_TAG', pos: 'xs2', type: 'text', visible: true, readonly: false },
        { label: 'Cap Date', elm: 'CAP_DATE', pos: 'xs2', type: 'date', visible: true, readonly: false },
      ],
    ],
  },
  mutations: {
    upUserCredentials(state, value) {
      let tmp = JSON.parse(value)
      tmp.RPTS = JSON.parse(tmp.RPTS)
      state.userCredentials = tmp
      // console.log(state.userCredentials.RPTS)
    },
    upUserDetails(state, value) {
      state.userDetails = JSON.parse(value)
    },
    upInstruments(state, value) {
      state.instruments = value
    },
    upDrops(state, value) {
      state.drops = value
    },
    upSort(state, value) {
      state.sortBy = value
    },
    upFilters(state, value) {
      state.cur_filters = value
    },
    upAllFilters(state, value) {
      state.all_filters = value
    },
    upPage(state, value) {
      state.page = value
    },
    upPanel(state, value) {
      state.panel = value
    },
    upSrchText(state, value) {
      state.search_text = value
    },
    upSrch(state, value) {
      let tmp = {}
      value.forEach(el => {
        const item = el.split('|')
        if (tmp[item[1]] == undefined) tmp[item[1]] = []
        tmp[item[1]].push(item[0])
      })
      state.srch_obj = tmp
    },
    upAddEditData(state, value) {
      state.addEditData = value
    },
    clrAddEditData(state) {
      state.addEditData = {}
    },
  },
  actions: {
    getUserCredentials({ commit }) {
      try {
        commit('upUserCredentials', Cookies.get('user_credentials'))
        commit('upUserDetails', Cookies.get('user_details'))
      } catch (error) {
        console.log(error.message)
      }
    },
    getInstruments({ state, commit }) {
      let page = state.page - 1
      let srh = ''

      commit('upPanel', -1)
      for (const key in state.srch_obj) {
        srh += `&${key}=${state.srch_obj[key].join('|').replace(/&/g, '%26')}`
      }

      axios
        .get(`/instruments?page=${page}&limit=${state.limit}${srh}&sort=${state.sortBy}&WHR=${state.search_text}`)
        .then(result => {
          commit('upFilters', result.data.filters)
          commit('upInstruments', result.data)
        })
        .catch(error => {
          // handle error
          console.log(error)
        })
    },
    getLogByInstId({ state }, id) {
      return new Promise((resolve, reject) => {
        axios
          .get('/logs/' + id)
          .then(result => {
            resolve(result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    userLogin({ state }, credentials) {
      return new Promise((resolve, reject) => {
        axios
          .post('/users/login', {
            username: credentials.username,
            password: credentials.password,
          })
          .then(result => {
            resolve(result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    getCSV({ state }) {
      let srh = ''

      for (const key in state.srch_obj) {
        srh += `&${key}=${state.srch_obj[key].join('|').replace(/&/g, '%26')}`
      }

      axios
        .get(`/csv?${srh}&sort=${state.sortBy}&WHR=${state.search_text}`, {
          responseType: 'arraybuffer',
        })
        .then(response => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]))
          var fileLink = document.createElement('a')

          fileLink.href = fileURL
          fileLink.setAttribute('download', `INVENTORY - ${moment().format('MMM-DD-YY')}.csv`)
          document.body.appendChild(fileLink)

          fileLink.click()
        })
        .catch(error => {
          // handle error
          console.log(error)
        })
    },
    getCSVperAcc() {
      axios
        .get(`/csv/peraccount`, {
          responseType: 'arraybuffer',
        })
        .then(response => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]))
          var fileLink = document.createElement('a')

          fileLink.href = fileURL
          fileLink.setAttribute('download', `IB per Account - ${moment().format('MMM-DD-YY')}.csv`)
          document.body.appendChild(fileLink)

          fileLink.click()
        })
        .catch(error => {
          // handle error
          console.log(error)
        })
    },
    getInvRpt({ state }) {
      let srh = ''

      for (const key in state.srch_obj) {
        srh += `&${key}=${state.srch_obj[key].join('|').replace(/&/g, '%26')}`
      }

      axios
        .get(`/csv/inventory_rpt?${srh}&sort=${state.sortBy}&WHR=${state.search_text}`, {
          responseType: 'arraybuffer',
        })
        .then(response => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]))
          var fileLink = document.createElement('a')

          fileLink.href = fileURL
          fileLink.setAttribute('download', `ALL DATA - ${moment().format('MMM-DD-YY')}.csv`)
          document.body.appendChild(fileLink)

          fileLink.click()
        })
        .catch(error => {
          // handle error
          console.log(error)
        })
    },
    getTSGrpt({ state }) {
      let srh = ''

      for (const key in state.srch_obj) {
        srh += `&${key}=${state.srch_obj[key].join('|').replace(/&/g, '%26')}`
      }

      axios
        .get(`/csv/tsg_report?${srh}&sort=${state.sortBy}&WHR=${state.search_text}`, {
          responseType: 'arraybuffer',
        })
        .then(response => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]))
          var fileLink = document.createElement('a')

          fileLink.href = fileURL
          fileLink.setAttribute('download', `TSG REPORT - ${moment().format('MMM-DD-YY')}.csv`)
          document.body.appendChild(fileLink)

          fileLink.click()
        })
        .catch(error => {
          // handle error
          console.log(error)
        })
    },
    getFilters({ commit }) {
      axios
        .get(`/instruments/filters`)
        .then(result => {
          commit('upAllFilters', result.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    getDropDowns({ commit }) {
      axios
        .get(`/drops`)
        .then(result => {
          commit('upDrops', result.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    postInstrumentData({ state }) {
      let method = ''
      let url = '/instruments'
      let data = state.addEditData
      data.chgBy = state.userDetails.username
      if (data.id !== undefined) {
        method = 'patch'
        url += `/${data.id}`
      } else {
        method = 'post'
      }
      for (const key in data) {
        if (data[key] == undefined) {
          data[key] = ''
        }
      }
      // console.log(`${method} - ${url}`)
      axios({
        method: method,
        url: url,
        data: data,
      })
    },
    postNewPassword({ state }, data) {
      return new Promise((resolve, reject) => {
        axios
          .post('/users/change-password', {
            username: data.username,
            password: data.curpass,
            NewPassword: data.newpassword,
          })
          .then(res => {
            resolve(res)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
  },
  getters: {
    getSrchItems: state => {
      let srch_array = []
      for (const key in state.srch_obj) {
        if (state.srch_obj.hasOwnProperty(key)) {
          state.srch_obj[key].forEach(el => {
            srch_array.push(`${el}|${key}`)
          })
        }
      }
      return srch_array
    },
  },
})
